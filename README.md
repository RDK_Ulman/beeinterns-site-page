# BeeInterns Site-Page

В этом проекте представлен собранный макет страницы вебсайта **Beeinterns**. 
**Макет сайта**: https://www.figma.com/file/amqTMRujXElnp2ou657Ysd/beeinterns_test?node-id=0%3A1

Проект реализован на сборке Gulp. Для запуска необходимо открыть проект в VSCode, в терминале набрать команду `npm i`, чтобы установить все используемые в сборке плагины. Далее, проект можно запустить при помощи команды `gulp`. 

Дополнительные команды сборки: 
- `gulp build` - команда собирает проект и помещает все файлы в папку app, в которой находятся папки css и js (с минимизированными файлами), img. Папки с favicon, шрифтами и т.д. тоже помещаются в общую дерикторию папки app. 
-  `gulp backend` - команда собирает проект таким образом, чтобы потом его можно было передать бэкэндеру

Все файлы проекта располагаются в директории папки src.
Структура папки src:
```bash
src
├───img
│   ├───lections
│   └───svg
├───js
│   ├───components
│   └───vendor
├───partials
├───resources
│   ├───favicons
│   └───fonts
└───scss
    ├───components
    ├───mixins
    └───vendor 
 ```

В папке **img** картинки содержатся либо в общей директории, либо в различных тематических папках. Все SVG файлы для создания спрайтов помещаются в папку svg.

В папке **js** 2 папки: `components` и `vendor`. В папке `vendor` размещаются минимизированный файлы каких-либо библиотек (н-р, библиотека sviper). 

В папке `components` размещаются *.js файлы, необходимые для работы тех или иных элементов на сайте. 

В папке  **partials** содержатся html файлы. В сборке подключен плагин include-files. Поэтому в этой файле содержатся блоки html кода, которые затем можно подключать на страницу. (Пример: в файле header.html реализована разметка шапки сайта. header.html располагается в partials. Потом на страницу сайта index.html этот header подключается через `@include('partials/header.html'`))

В папке **resourses** содержатся шрифты, фавиконки, видео и прочие файлы.

В папке **scss** 3 папки: `components`, `vendor`, `mixins`. В `components`размещены *.scss файлы со стилями.































## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/RDK_Ulman/beeinterns-site-page.git
git branch -M main
git push -uf origin main
```
